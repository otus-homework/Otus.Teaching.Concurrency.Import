﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.WebApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _repository;

        public CustomerService(ICustomerRepository repository)
        {
            _repository = repository;
        }

        public async Task<ResultWrap<Customer>> GetCustomer(int id)
        {
            var customer = await _repository.GetCustomer(id);
            if (customer == null)
            {
                return new ResultWrap<Customer>(ResultValues.NotFound, null);
            }
            return new ResultWrap<Customer>(ResultValues.Success, customer);
        }

        public async Task<ResultWrap<Customer>> AddCustomer(Customer customer)
        {
            var result = await GetCustomer(customer.Id);
            if (result.ResultObject != null)
            {
                return new ResultWrap<Customer>(ResultValues.Conflict, null);
            }
            await _repository.AddCustomer(customer);
            return new ResultWrap<Customer>(ResultValues.Success, customer);
        }
    }
}
