﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebApi.Model;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Services
{
    public interface ICustomerService
    {
        Task<ResultWrap<Customer>> AddCustomer(Customer customer);
        Task<ResultWrap<Customer>> GetCustomer(int id);
    }
}