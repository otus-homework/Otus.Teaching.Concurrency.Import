﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebApi.Model;
using Otus.Teaching.Concurrency.Import.WebApi.Services;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        [Route("GetCustomer/{id}")]
        public async Task<ActionResult<Customer>> GetCustomer(int id)
        {
            var result = await _customerService.GetCustomer(id);
            switch (result.ResultValue)
            {
                case ResultValues.Success:
                    return Ok(result.ResultObject);
                case ResultValues.NotFound:
                    return NotFound();
                default: 
                    return BadRequest();
            }
        }

        [HttpPost]
        [Route("AddCustomer")]
        public async Task<ActionResult> AddCustomer(Customer customer)
        {
            var result = await _customerService.AddCustomer(customer);
            switch (result.ResultValue)
            {
                case ResultValues.Success:
                    return Ok(result.ResultObject);
                case ResultValues.Conflict:
                    return Conflict($"Customer with id {customer.Id} already exists");
                default:
                    return BadRequest();
            }
        }
    }
}