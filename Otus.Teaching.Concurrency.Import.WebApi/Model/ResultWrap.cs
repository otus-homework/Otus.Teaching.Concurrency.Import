﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Model
{
    public enum ResultValues
    {
        Success,
        NotFound,
        Conflict
    }
    public class ResultWrap<T>
    {
        public ResultValues ResultValue { get; set; }
        public T ResultObject { get; set; }

        public ResultWrap(ResultValues resultValue, T resultObject)
        {
            ResultValue = resultValue;
            ResultObject = resultObject;
        }
    }
}
