﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Helpers;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly IEnumerable<XElement> _dataList;
        public XmlParser(IEnumerable<XElement> dataList)
        {
            _dataList = dataList;
        }
        public List<Customer> Parse()
        {
            List<Customer> customers = new List<Customer>();
            foreach (var item in _dataList)
            {
                var customer = DeserializeXmlCustomer(item);
                customers.Add(customer);
            }

            return customers;
        }

        private Customer DeserializeXmlCustomer(XElement xElement)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Customer));
            return (Customer)serializer.Deserialize(xElement.CreateReader());
        }
    }
}