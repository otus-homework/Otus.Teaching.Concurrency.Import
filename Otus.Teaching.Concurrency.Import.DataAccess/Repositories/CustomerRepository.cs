using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DesignTimeDbContextFactory _contextFactory;

        public CustomerRepository(DesignTimeDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public void AddCustomers(List<Customer> customers)
        {
            if (customers == null)
                throw new ArgumentNullException(nameof(customers));

            using (var context = _contextFactory.CreateDbContext(null))
            {
                foreach (var customer in customers)
                {
                    var exists = context.Customers.Find(customer.Id);
                    if (exists == null)
                        context.Customers.Add(customer);
                }

                using (var transaction = context.Database.BeginTransaction())
                {
                    context.Database.OpenConnection();
                    try
                    {
                        context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Customers ON");
                        context.SaveChanges();
                        context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT Customers OFF");
                        transaction.Commit();
                    }
                    finally
                    {
                        context.Database.CloseConnection();
                    }
                }
            }
        }

        public async Task AddCustomersAsync(List<Customer> customers)
        {
            if (customers == null)
                throw new ArgumentNullException(nameof(customers));

            using (var context = _contextFactory.CreateDbContext(null))
            {
                foreach (var customer in customers)
                {
                    var exists = context.Customers.Find(customer.Id);
                    if (exists == null)
                        await context.Customers.AddAsync(customer);
                }

                await SaveChangesAsync(context);
            }
        }

        private static async Task SaveChangesAsync(CustomerDbContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                await context.Database.OpenConnectionAsync();
                try
                {
                    await context.Database.ExecuteSqlRawAsync("SET IDENTITY_INSERT Customers ON");
                    await context.SaveChangesAsync();
                    await context.Database.ExecuteSqlRawAsync("SET IDENTITY_INSERT Customers OFF");
                    await transaction.CommitAsync();
                }
                finally
                {
                    await context.Database.CloseConnectionAsync();
                }
            }
        }

        public async Task<Customer> GetCustomer(int id)
        {
            using var context = _contextFactory.CreateDbContext(null);
            return await context.Customers.FindAsync(id);
        }

        public async Task AddCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            using var context = _contextFactory.CreateDbContext(null);

            var exists = context.Customers.Find(customer.Id);
            if (exists == null)
                context.Customers.Add(customer);

            await SaveChangesAsync(context);
        }
    }
}