﻿using Otus.Teaching.Concurrency.Import.Core.Helpers;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    class XmlDataLoaderOnThreads : IDataLoader
    {
        private readonly string _fileName;
        private readonly int _threadsCount;
        private readonly ICustomerRepository _customerRepository;

        public XmlDataLoaderOnThreads(ICustomerRepository customerRepository, string fileName, int threadsCount = 4)
        {
            _fileName = fileName;
            _threadsCount = threadsCount;
            _customerRepository = customerRepository;
        }

        public void LoadData()
        {
            var xdoc = XDocument.Load(_fileName);
            var xElementList = xdoc.Element("Customers").Element("Customers").Elements("Customer");
            var chunks = xElementList.ChunkBy(_threadsCount);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            LoadOnThreads(chunks);
            sw.Stop();
            Console.WriteLine($"Загрузка с использование Thread за {sw.ElapsedMilliseconds} ms");
        }

        private void LoadOnThreads(IEnumerable<IEnumerable<XElement>> chunks)
        {
            List<Thread> threads = new List<Thread>(_threadsCount);

            foreach (var chunk in chunks)
            {
                var thread = new Thread(() => ThreadChunkProcess(chunk));
                threads.Add(thread);
                thread.Start();
            }

            while (threads.Any(t => t.IsAlive)) { }
        }

        /// <summary>
        /// Вся работа потока для одного пакета данных
        /// </summary>
        /// <param name="chunk">Пакет данных</param>
        private void ThreadChunkProcess(IEnumerable<XElement> chunk)
        {
            var customers = new XmlParser(chunk).Parse();
            _customerRepository.AddCustomers(customers);
        }
    }
}
