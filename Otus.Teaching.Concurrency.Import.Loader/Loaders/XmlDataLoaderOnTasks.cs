﻿using Otus.Teaching.Concurrency.Import.Core.Helpers;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    class XmlDataLoaderOnTasks : IDataLoader
    {
        private readonly string _fileName;
        private readonly int _threadsCount;
        private readonly ICustomerRepository _customerRepository;

        public XmlDataLoaderOnTasks(ICustomerRepository customerRepository, string fileName, int threadsCount = 4)
        {
            _fileName = fileName;
            _threadsCount = threadsCount;
            _customerRepository = customerRepository;
        }

        public void LoadData()
        {
            var xdoc = XDocument.Load(_fileName);
            var xElementList = xdoc.Element("Customers").Element("Customers").Elements("Customer");
            var chunks = xElementList.ChunkBy(_threadsCount);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            LoadOnThreads(chunks);
            sw.Stop();
            Console.WriteLine($"Загрузка с использование Thread за {sw.ElapsedMilliseconds} ms");
        }

        private void LoadOnThreads(IEnumerable<IEnumerable<XElement>> chunks)
        {
            List<Task> tasks = new List<Task>();

            foreach (var chunk in chunks)
            {
                var task = new Task(async () => await ThreadChunkProcess(chunk));
                tasks.Add(task);
                task.Start();
            }
            Task.WaitAll(tasks.ToArray());
        }

        /// <summary>
        /// Вся работа потока для одного пакета данных
        /// </summary>
        /// <param name="chunk">Пакет данных</param>
        private async Task ThreadChunkProcess(IEnumerable<XElement> chunk)
        {
            var customers = new XmlParser(chunk).Parse();
            await _customerRepository.AddCustomersAsync(customers);
        }
    }
}
