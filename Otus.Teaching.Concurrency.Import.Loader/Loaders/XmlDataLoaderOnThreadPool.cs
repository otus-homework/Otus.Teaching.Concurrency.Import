﻿using Bogus;
using Otus.Teaching.Concurrency.Import.Core.Helpers;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using static Otus.Teaching.Concurrency.Import.Loader.Loaders.XmlDataLoaderOnThreadPool;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class XmlDataLoaderOnThreadPool : IDataLoader
    {
        private readonly string _fileName;
        private readonly int _threadsCount;
        private readonly ICustomerRepository _customerRepository;
        List<ManualResetEvent> events = new List<ManualResetEvent>();

        public XmlDataLoaderOnThreadPool(ICustomerRepository customerRepository, string fileName, int threadsCount = 4)
        {
            _fileName = fileName;
            _threadsCount = threadsCount;
            _customerRepository = customerRepository;
        }

        public void LoadData()
        {
            var xdoc = XDocument.Load(_fileName);
            var xElementList = xdoc.Element("Customers").Element("Customers").Elements("Customer");
            var chunks = xElementList.ChunkBy(_threadsCount);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            LoadOnThreadPool(chunks);
            sw.Stop();
            Console.WriteLine($"Загрузка с использование ThreadPool за {sw.ElapsedMilliseconds} ms");
        }

        private void LoadOnThreadPool(IEnumerable<IEnumerable<XElement>> chunks)
        {
            foreach (var chunk in chunks)
            {
                var manualResetEvent = new ManualResetEvent(false);
                ThreadPoolObject threadPollObject = new ThreadPoolObject
                {
                    Chunk = chunk,
                    Signal = manualResetEvent
                };
                events.Add(manualResetEvent);
                ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadPoolChunkProcess), threadPollObject);
            }
            WaitForAll(events.ToArray());
        }

        /// <summary>
        /// Вся работа потока для одного пакета данных
        /// </summary>
        /// <param name="chunk">Пакет данных</param>
        private void ThreadPoolChunkProcess(object obj)
        {
            var threadPoolObject = obj as ThreadPoolObject;

            var customers = new XmlParser(threadPoolObject.Chunk).Parse();
            _customerRepository.AddCustomers(customers);

            threadPoolObject.Signal.Set();
        }

        /// <summary>
        /// Ожидание выполнения потоков
        /// </summary>
        /// <param name="events">Список сигналов в потоках</param>
        /// <returns></returns>
        private bool WaitForAll(ManualResetEvent[] events)
        {
            bool result = false;
            try
            {
                if (events != null)
                {
                    for (int i = 0; i < events.Length; i++)
                    {
                        events[i].WaitOne();
                    }
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private class ThreadPoolObject
        {
            public ManualResetEvent Signal { get; set; }
            public IEnumerable<XElement> Chunk { get; set; }
        }

    }
}
