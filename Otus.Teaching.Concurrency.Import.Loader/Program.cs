﻿using System;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.DataGenerators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static IConfiguration _configuration;
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            ConfigureServices();

            //var generation = _serviceProvider.GetRequiredService<CustomDataGenerator>();
            var generation = _serviceProvider.GetRequiredService<AsyncCustomDataGenerator>();
            //generation.GenerateCustomersDataFile(null);
            generation.GenerateCustomersDataFile(new string[] { "-m" });

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            var loader = _serviceProvider.GetRequiredService<IDataLoader>();
            loader.LoadData();

            DisposeServices();
        }

        private static void ConfigureServices()
        {
            var serviceCollection = new ServiceCollection();

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json", false)
                .Build();
            serviceCollection.AddSingleton<IConfiguration>(_configuration);

            serviceCollection.AddTransient<IDataGenerator>((isp) =>
                new XmlGenerator(
                    Path.Combine(_configuration.GetSection("GeneratorPath").Value,
                                 _configuration.GetValue<string>("DataFileName") + _configuration.GetValue<string>("FileExtention")),
                    _configuration.GetValue<int>("GeneratorItemsCount"))
                );

            serviceCollection.AddTransient<CustomDataGenerator>();
            serviceCollection.AddTransient<AsyncCustomDataGenerator>();

            string connectionString = _configuration.GetConnectionString("DefaultConnection");
            serviceCollection.AddDbContext<CustomerDbContext>(options => options.UseSqlServer(connectionString), ServiceLifetime.Transient);
            serviceCollection.AddScoped<ICustomerRepository, CustomerRepository>();
            serviceCollection.AddSingleton<DesignTimeDbContextFactory>();

            //serviceCollection.AddSingleton<IDataLoader>(isp =>
            //    new XmlDataLoaderOnThreads(
            //        _serviceProvider.GetRequiredService<ICustomerRepository>(),
            //        Path.Combine(_configuration.GetValue<string>("GeneratorPath"),
            //                     _configuration.GetValue<string>("DataFileName") + _configuration.GetValue<string>("FileExtention")),
            //        _configuration.GetValue<int>("ThreadCount")
            //        )
            //    );

            //serviceCollection.AddSingleton<IDataLoader>(isp =>
            //    new XmlDataLoaderOnThreadPool(
            //        _serviceProvider.GetRequiredService<ICustomerRepository>(),
            //        Path.Combine(_configuration.GetValue<string>("GeneratorPath"),
            //                     _configuration.GetValue<string>("DataFileName") + _configuration.GetValue<string>("FileExtention")),
            //        _configuration.GetValue<int>("ThreadCount")
            //        )
            //    );

            serviceCollection.AddSingleton<IDataLoader>(isp =>
                new XmlDataLoaderOnTasks(
                    _serviceProvider.GetRequiredService<ICustomerRepository>(),
                    Path.Combine(_configuration.GetValue<string>("GeneratorPath"),
                                 _configuration.GetValue<string>("DataFileName") + _configuration.GetValue<string>("FileExtention")),
                    _configuration.GetValue<int>("ThreadCount")
                    )
                );

            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable)
            {
                ((IDisposable)_serviceProvider).Dispose();
            }
        }
    }
}