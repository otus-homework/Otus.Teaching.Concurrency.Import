﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.DataGenerators
{
    public class AsyncCustomDataGenerator
    {
        private readonly IConfiguration _configuration;
        private readonly IDataGenerator _dataGenerator;

        public AsyncCustomDataGenerator(IConfiguration configuration, IDataGenerator dataGenerator)
        {
            _configuration = configuration;
            _dataGenerator = dataGenerator;
        }

        public void GenerateCustomersDataFile(string[] args)
        {
            if (args == null)
            {
                Console.WriteLine("Нет аргументов запуска");
                return;
            }
            var generatorPath = _configuration.GetSection("GeneratorPath").Value;
            var generatorFileName = _configuration.GetSection("GeneratorFileName").Value;
            var generatorItemsCount = _configuration.GetSection("GeneratorItemsCount").Value;
            var dataFileName = _configuration.GetSection("DataFileName").Value;

            switch (args[0])
            {
                case "-m":
                default:
                    Console.WriteLine($"Старт генератора");
                    _dataGenerator.GenerateAsync().GetAwaiter().GetResult();
                    break;
                case "-p":
                case "":
                    Process process = new Process();
                    process.StartInfo.FileName = Path.Combine(generatorPath, generatorFileName);
                    process.StartInfo.Arguments = $"{dataFileName} {generatorItemsCount}";
                    process.Start();
                    Console.WriteLine($"Старт генератора, процесс: {process.Id}");
                    process.WaitForExit();
                    if (process.ExitCode == 0)
                        Console.WriteLine($"Генерация успешно заверешена!");
                    else
                        Console.WriteLine($"Генерация прервана: {process.ExitCode}");
                    break;
            }
        }
    }
}
