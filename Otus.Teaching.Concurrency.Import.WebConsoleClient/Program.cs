﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebConsoleClient
{
    class Program
    {
        private static IConfiguration _configuration;
        static void Main(string[] args)
        {
            _configuration = GetConfigation();
            Console.Write("Press GET or POST method to use: ");
            try
            {
                switch (Console.ReadLine().ToUpper())
                {
                    case "GET":
                        GetCustomer();
                        break;
                    case "POST":
                        AddCustomer();
                        break;
                    default:
                        Console.WriteLine("Wrong command!");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex}");
            }
            Console.ReadKey();
        }

        private static async void GetCustomer()
        {
            Console.Write("GET: Enter Customer Id: ");
            if (!int.TryParse(Console.ReadLine(), out int id))
                return;

            using var client = new HttpClient();
            var result = await client.GetAsync($"{_configuration["ApiEndpoint"]}/Customer/GetCustomer/{id}");

            Console.WriteLine($"StatusCode: {result.StatusCode}");
            Console.WriteLine($"Result: {await result.Content.ReadAsStringAsync()}");
        }

        private async static void AddCustomer()
        {
            Console.Write("POST: Enter Customer Id: ");
            if (!int.TryParse(Console.ReadLine(), out int id))
                return;

            var customer = RandomCustomerGenerator.GenerateCustomerById(id);
            var content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");

            using var client = new HttpClient();
            var result = await client.PostAsync($"{_configuration["ApiEndpoint"]}/Customer/AddCustomer", content);

            Console.WriteLine($"StatusCode: {result.StatusCode}");
            Console.WriteLine($"Result: {await result.Content.ReadAsStringAsync()}");
        }

        private static IConfiguration GetConfigation()
        {
            return new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();
        }
    }
}
