using NUnit.Framework;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.WebApi.Model;
using Otus.Teaching.Concurrency.Import.WebApi.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Tests
{
    public class Tests
    {
        private ICustomerService _customerService;

        [SetUp]
        public void Setup()
        {
            _customerService = new FakeCustomerService();
        }

        [TestCase(1)]
        public async Task CustomerService_CorrectCustomer_ReturnSuccess(int id)
        {
            var result = await _customerService.GetCustomer(id);
            Assert.NotNull(result.ResultObject);
            Assert.AreEqual(ResultValues.Success, result.ResultValue);
        }

        [TestCase(5)]
        public async Task CustomerService_UncorrectCustomer_ReturnNotFound(int id)
        {
            var result = await _customerService.GetCustomer(id);
            Assert.AreEqual(ResultValues.NotFound, result.ResultValue);
        }

        [TestCase(2)]
        public async Task CustomerService_CustomerWithMatchingId_ReturnConflict(int id)
        {
            var customer = RandomCustomerGenerator.GenerateCustomerById(id);
            var result = await _customerService.AddCustomer(customer);
            Assert.AreEqual(ResultValues.Conflict, result.ResultValue);
        }
    }
}