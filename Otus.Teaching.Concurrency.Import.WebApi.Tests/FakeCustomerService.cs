﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.WebApi.Model;
using Otus.Teaching.Concurrency.Import.WebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Tests
{
    class FakeCustomerService : ICustomerService
    {
        public List<Customer> _fakeReposetory;
        public FakeCustomerService()
        {
            _fakeReposetory = new List<Customer>()
            {
                new Customer() { Id = 1, FullName = "Test1", Email = "test1@gmail.com", Phone = "1-876-987-098" },
                new Customer() { Id = 2, FullName = "Test2", Email = "test2@gmail.com", Phone = "2-123-234-345" },
                new Customer() { Id = 3, FullName = "Test3", Email = "test3@gmail.com", Phone = "3-321-432-543" },
            };
        }

        public async Task<ResultWrap<Customer>> AddCustomer(Customer customer)
        {
            var result = await GetCustomer(customer.Id);
            if (result.ResultObject != null)
            {
                return new ResultWrap<Customer>(ResultValues.Conflict, null);
            }
            _fakeReposetory.Add(customer);
            return new ResultWrap<Customer>(ResultValues.Success, customer);
        }

        public async Task<ResultWrap<Customer>> GetCustomer(int id)
        {
            var customer = _fakeReposetory.FirstOrDefault(x => x.Id == id);
            if (customer == null)
            {
                return new ResultWrap<Customer>(ResultValues.NotFound, null);
            }
            return new ResultWrap<Customer>(ResultValues.Success, customer);
        }
    }
}
