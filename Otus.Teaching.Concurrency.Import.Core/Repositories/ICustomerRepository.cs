using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomers(List<Customer> customers);
        Task AddCustomersAsync(List<Customer> customers);
        Task<Customer> GetCustomer(int customerId);
        Task AddCustomer(Customer customer);
    }
}